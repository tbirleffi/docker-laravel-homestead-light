#!/bin/bash

ENV="$1";
PRJ="$2";
source ~/.bash_profile;

runDockerComposeTeardown()
{
	if [ "$ENV" = "" ]
	then
		echo "Please provide the environment: local, dev, stage, prod, jenkins";
		exit 1;
	fi

	if [[ "$PRJ" = "" && "$ENV" != "local" ]]
	then
		echo "Please provide the environment project name: (ex: MyProjDev)";
		exit 1;
	fi

	if [ "$ENV" != "" ]
	then
		stopServices;
	fi
}

stopServices()
{
	if [ "$PRJ" != "" ]
	then
		docker-compose -p "$PRJ" stop;
	else
		docker-compose stop;
	fi
}


runDockerComposeTeardown;

